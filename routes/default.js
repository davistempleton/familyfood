module.exports = (config) => {
    // Pull in the required modules.
    const express = require('express');
    
    // Get the router.
    const router = express.Router();
    
    router.get('/', (req, res) => {
        res.render('index.njk', {
            site: config.site,
        });
    });
    
    return router;
};
