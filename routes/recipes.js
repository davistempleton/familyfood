module.exports = (config) => {
        const express = require('express');
        
        const router = express.Router();
        
        router.get('/', (req, res) => {
            // Get all the recipes.
            res.render('recipes/list.njk', {
                site: config.site
            });
        });
        
        router.get('/:slug', (req, res) => {
            const slug = req.params.slug;
            
            res.render('recipes/details.njk', {
                site: config.site
            });
        });
        
        return router;
};