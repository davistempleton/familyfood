module.exports = (config) => {
        const express = require('express');
        const router = express.Router();
        
        router.get(/.*/, (req, res) => {
            res.render('error.njk', {
                site: config.site,
                page: {
                    title: "Error"
                }
            });
        });
        
        return router;
};