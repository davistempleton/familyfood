// Required modules
const express = require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const stylus = require('stylus');

// Turn on the app
const app = express();

// Pull in the site configs
const config = require(path.join(__dirname, 'config.json'));

// Get nunjucks going
nunjucks.configure('views', {
    autoescape: false,
    noCache: config.debug,
    express: app
});

// Turn on public folder
app.use(express.static('public'));

// Routes
const routes = {
        default: require(path.join(__dirname, 'routes/default'))(config),
        recipes: require(path.join(__dirname, 'routes/recipes'))(config),
        error: require(path.join(__dirname, 'routes/error'))(config)
};

// Define the project wide urls.
app.use('/', routes.default);
app.use('/recipes', routes.recipes);
app.use(/.*/, routes.error);

// Turn on the app to listen.
const port = config.debug ? config.port.dev : config.port.prod;
app.listen(port, () => {
    console.log(config.site.title + ' is listening on ' + port);
});
